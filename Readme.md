Mulity GPS Starter
====
----
目录结构:

	预览版：
	MulityGPS_NoBackgrnd 第一个不带启动画面版本，导航二次启动方式为设置窗口最前（导航最小化后无法再次正常启动）；
	MulityGPS_Splash 第一个带启动画面版本，启动速度较慢。（带背景图片，耗费较长启动时间）
	修正版：
	MulityGPS_Fix 修正启动方式，禁用以前的设置窗口最前，避免导航最小化之后无法正常从新启动；
	MulityGPS_NoDetect 去掉主程序启动时的导航是否已经运行检测，将检测功能独立出来，对应目录为Loader；
	Loader 检测导航是否已经启动，如果启动将再次执行导航程序，如果没有启动激活MulityGPS_NoDetect主程序进行导航系统选择。

---
截图：

最新界面：

![](https://bytebucket.org/obaby/mulity-gps-starter/raw/dda5b07f5008d7aa01bab2bb3f5066fa7617e28b/screen.png)

----

以下为旧版界面：

启动画面：

![](https://bytebucket.org/obaby/mulity-gps-starter/raw/dda5b07f5008d7aa01bab2bb3f5066fa7617e28b/Splash.png)

桌面：

![](https://bytebucket.org/obaby/mulity-gps-starter/raw/dda5b07f5008d7aa01bab2bb3f5066fa7617e28b/Splash_view.png)

无Splash界面：

![](https://bytebucket.org/obaby/mulity-gps-starter/raw/dda5b07f5008d7aa01bab2bb3f5066fa7617e28b/Nospalsh.png)

	1.两个版本，分别为不带Splash和背景图片的的MulityGPS_NoBackgrnd和带Splash以及背景图片的MulityGPS_Splash
	2.提供的小工具：任务管理器，内存释放，Spy++，系统信息查看，total commander
	3.功能：显示桌面
	4.支持的两个地图高德，凯立德，图吧，dsa。多个地图


由于程序编译体积较大，启动的时候需要一定的时间，我的车载导航只有128的内存，每次在切换的时候都需要一定时间进行启动。所以略微有些郁闷。


编译环境安装：[http://www.h4ck.org.cn/2014/05/%e5%9f%ba%e4%ba%8elazarus-%e7%9a%84win-ce%e5%bc%80%e5%8f%91/](http://www.h4ck.org.cn/2014/05/%e5%9f%ba%e4%ba%8elazarus-%e7%9a%84win-ce%e5%bc%80%e5%8f%91/)
