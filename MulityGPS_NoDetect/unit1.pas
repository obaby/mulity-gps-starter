unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, RTTICtrls, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, StdCtrls, Windows, IniFiles;

type

  { TForm1 }

  TForm1 = class(TForm)
    Image_mapbar: TImage;
    Image_kailide2: TImage;
    Image_gaode2: TImage;
    Image3: TImage;
    Image_Totalcommander: TImage;
    Image_clear: TImage;
    Image_kailide: TImage;
    Image_desktop: TImage;
    Image_taskmgr: TImage;
    Image_gaode: TImage;
    Image_smile: TImage;
    Image_setting: TImage;
    Image_dsa: TImage;
    Image_welcome: TImage;
    Image_systeminfo: TImage;
    Image9: TImage;
    Image_Exit: TImage;
    Label_DateTime: TLabel;
    Timer1: TTimer;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
    procedure Image3Click(Sender: TObject);
    procedure Image6Click(Sender: TObject);
    procedure Image9Click(Sender: TObject);
    procedure Image_clearClick(Sender: TObject);
    procedure Image_desktopClick(Sender: TObject);
    procedure Image_dsaClick(Sender: TObject);
    procedure Image_ExitClick(Sender: TObject);
    procedure Image_gaode2Click(Sender: TObject);
    procedure Image_gaodeClick(Sender: TObject);
    procedure Image_kailide2Click(Sender: TObject);
    procedure Image_kailideClick(Sender: TObject);
    procedure Image_mapbarClick(Sender: TObject);
    procedure Image_settingClick(Sender: TObject);
    procedure Image_smileClick(Sender: TObject);
    procedure Image_systeminfoClick(Sender: TObject);
    procedure Image_taskmgrClick(Sender: TObject);
    procedure Image_TotalcommanderClick(Sender: TObject);
    procedure Image_welcomeClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);

  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }


procedure exec();
var
   ShExecInfo: SHELLEXECUTEINFO;
begin
   ShExecInfo.cbSize:= sizeof(SHELLEXECUTEINFO);
   ShExecInfo.fMask:= SEE_MASK_NOCLOSEPROCESS;
   ShExecInfo.hwnd:= NULL;
   ShExecInfo.lpVerb:='';
   ShExecInfo.lpFile:='\SDMMC\MulityGPS\bin\TaskMan.exe';
   ShExecInfo.lpParameters:='';
   ShExecInfo.lpDirectory:='';
   ShExecInfo.nShow:= SW_SHOW;
   ShExecInfo.hInstApp := NULL;
   ShellExecuteEx(@ShExecInfo);
   //dwret:=WaitForSingleObject(ShExecInfo.hProcess,INFINITE);  //等待结束
   CloseHandle(ShExecInfo.hProcess);
end;

procedure MyExecuteFile(BinFileName:PWideChar);
var
   ShExecInfo: SHELLEXECUTEINFO;
begin
   ShExecInfo.cbSize:= sizeof(SHELLEXECUTEINFO);
   ShExecInfo.fMask:= SEE_MASK_NOCLOSEPROCESS;
   ShExecInfo.hwnd:= NULL;
   ShExecInfo.lpVerb:='';
   ShExecInfo.lpFile:=BinFileName;
   ShExecInfo.lpParameters:='';
   ShExecInfo.lpDirectory:='';
   ShExecInfo.nShow:= SW_SHOW;
   ShExecInfo.hInstApp := NULL;
   ShellExecuteEx(@ShExecInfo);
   //dwret:=WaitForSingleObject(ShExecInfo.hProcess,INFINITE);  //等待结束
   CloseHandle(ShExecInfo.hProcess);
end;

procedure TForm1.Image1Click(Sender: TObject);
begin

end;

procedure TForm1.Image2Click(Sender: TObject);
begin

end;

procedure TForm1.Image3Click(Sender: TObject);
begin
  close;
end;

procedure TForm1.Image6Click(Sender: TObject);
begin
  //SysUtils.ExecuteProcess('\SDMMC\MulityGPS\bin\TaskMan.exe',[]);
end;

procedure TForm1.Image9Click(Sender: TObject);
var
   MyCorePlayerFileName:string;
   MyWideCorePlayerFileFileName: array [0..256]of WChar;
begin
   MyCorePlayerFileName := ExtractFilePath(Paramstr(0)) +'bin\Coreplayer\player.exe';
   if FileExists(MyCorePlayerFileName) then begin
      StringToWideChar(MyCorePlayerFileName,MyWideCorePlayerFileFileName,length(MyCorePlayerFileName)+1);
      MyExecuteFile(MyWideCorePlayerFileFileName);
   end;
end;

procedure TForm1.Image_clearClick(Sender: TObject);
var
   MyFreeMemFileName:string;
   MyWideFreeMemFileName: array [0..256]of WChar;
begin
   MyFreeMemFileName := ExtractFilePath(Paramstr(0)) +'bin\FreeMem.exe';
   if FileExists(MyFreeMemFileName) then begin
      StringToWideChar(MyFreeMemFileName,MyWideFreeMemFileName,length(MyFreeMemFileName)+1);
      MyExecuteFile(MyWideFreeMemFileName);
   end;
end;

procedure TForm1.Image_taskmgrClick(Sender: TObject);
var
  MyTaskMgrBinFileName:string;
  //AProcess:TProcess;
  //MyWideTaskMgrBinFileName:PWideChar;
  MyWideTaskMgrBinFileName:array[0..256] of Widechar;
begin
  MyTaskMgrBinFileName:= ExtractFilePath(Paramstr(0)) + 'bin\TaskMan.exe';
  //ShellExecute(0,nil, PChar(MyTaskMgrBinFileName),PChar(''),nil,1) ;
  //RunCommand(MyTaskMgrBinFileName,nil,s);
  //SysUtils.ExecuteProcess(UTF8ToSys(MyTaskMgrBinFileName), '', []);
  //SysUtils.ExecuteProcess(MyTaskMgrBinFileName,[]);
  //SysUtils.ExecuteProcess('\SDMMC\MulityGPS\bin\TaskMan.exe',[]);
  //exec();
  //MyExecuteFile('\SDMMC\MulityGPS\bin\TaskMan.exe');
  if FileExists(MyTaskMgrBinFileName) then begin
     StringToWideChar(MyTaskMgrBinFileName,MyWideTaskMgrBinFileName,length(MyTaskMgrBinFileName)+1);
     MyExecuteFile(MyWideTaskMgrBinFileName);
  end;
  {ShowMessage(MyTaskMgrBinFileName);
  AProcess:=TProcess.Create(nil);
  AProcess.CommandLine := MyTaskMgrBinFileName;
  AProcess.Options := [poNewProcessGroup];
  AProcess.Execute;
  AProcess.Free;}
end;

procedure TForm1.Image_TotalcommanderClick(Sender: TObject);
var
   MyTotalCommandFileName:string;
   MyWideTotalCommandFileName: array [0..256]of WChar;
begin
   MyTotalCommandFileName := ExtractFilePath(Paramstr(0)) +'bin\TotalCommand.exe';
   if FileExists(MyTotalCommandFileName) then begin
      StringToWideChar(MyTotalCommandFileName,MyWideTotalCommandFileName,length(MyTotalCommandFileName)+1);
      MyExecuteFile(MyWideTotalCommandFileName);
   end;
end;

procedure TForm1.Image_welcomeClick(Sender: TObject);
var
   MyKailideFileName:string;
   MyWideKailideFileName: array [0..256]of WChar;
begin
   MyKailideFileName := ExtractFilePath(Paramstr(0)) +'NaviOneMG6\NaviOne.exe';
   if FileExists(MyKailideFileName) then begin
      StringToWideChar(MyKailideFileName,MyWideKailideFileName,length(MyKailideFileName)+1);
      MyExecuteFile(MyWideKailideFileName);
      //close;
   end;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  Label_DateTime.Caption:= DateToStr(now) +' ' + formatdatetime('hh:mm',time);
end;

procedure TForm1.Image_desktopClick(Sender: TObject);
begin
  SysUtils.ExecuteProcess('\windows\explorer.exe',[]);
end;

procedure TForm1.Image_dsaClick(Sender: TObject);
var
  MyDsaFileName:string;
  MyWideDsaFileName:array [0..256] of WChar;
begin
  MyDsaFileName :=  ExtractFilePath(Paramstr(0)) + 'DSA\DSA.exe';
  if FileExists(MyDsaFileName) then begin
     StringToWideChar(MyDsaFileName,MyWideDsaFileName,length(MyDsaFileName)+1);
     MyExecuteFile(MyWideDsaFileName);
  end;
end;

procedure TForm1.Image_ExitClick(Sender: TObject);
begin
  close;
end;

procedure TForm1.Image_gaode2Click(Sender: TObject);
var
   MyGaodeFileName:string;
   MyWideGaodeFileName: array [0..256]of WChar;
begin
   MyGaodeFileName := ExtractFilePath(Paramstr(0)) +'Autonavi1\RouCE6.exe';
   if FileExists(MyGaodeFileName) then begin
      StringToWideChar(MyGaodeFileName,MyWideGaodeFileName,length(MyGaodeFileName)+1);
      MyExecuteFile(MyWideGaodeFileName);
      //close;
   end;
end;

procedure TForm1.Image_gaodeClick(Sender: TObject);
var
   MyGaodeFileName:string;
   MyWideGaodeFileName: array [0..256]of WChar;
begin
   MyGaodeFileName := ExtractFilePath(Paramstr(0)) +'Autonavi\RouCE6.exe';
   if FileExists(MyGaodeFileName) then begin
      StringToWideChar(MyGaodeFileName,MyWideGaodeFileName,length(MyGaodeFileName)+1);
      MyExecuteFile(MyWideGaodeFileName);
      //close;
   end;
end;

procedure TForm1.Image_kailide2Click(Sender: TObject);
var
   MyKailideFileName:string;
   MyWideKailideFileName: array [0..256]of WChar;
begin
   MyKailideFileName := ExtractFilePath(Paramstr(0)) +'NaviOne1\NaviOne.exe';
   if FileExists(MyKailideFileName) then begin
      StringToWideChar(MyKailideFileName,MyWideKailideFileName,length(MyKailideFileName)+1);
      MyExecuteFile(MyWideKailideFileName);
      //close;
   end;
end;

procedure TForm1.Image_kailideClick(Sender: TObject);
var
   MyKailideFileName:string;
   MyWideKailideFileName: array [0..256]of WChar;
begin
   MyKailideFileName := ExtractFilePath(Paramstr(0)) +'NaviOne\NaviOne.exe';
   if FileExists(MyKailideFileName) then begin
      StringToWideChar(MyKailideFileName,MyWideKailideFileName,length(MyKailideFileName)+1);
      //image_kailide.Enabled:= false;
      MyExecuteFile(MyWideKailideFileName);
      //close;
   end;
end;

procedure TForm1.Image_mapbarClick(Sender: TObject);
var
   MyMapBarFileName:string;
   MyWideMapBarFileName: array [0..256]of WChar;
begin
   MyMapBarFileName := ExtractFilePath(Paramstr(0)) +'mapbar\MapbarNavi.exe';
   if FileExists(MyMapBarFileName) then begin
      StringToWideChar(MyMapBarFileName,MyWideMapBarFileName,length(MyMapBarFileName)+1);
      MyExecuteFile(MyWideMapBarFileName);
      //Image_mapbar.Enabled:=false;
      //Image_mapbar.Transparent:= true;
      //close;
   end;
end;

procedure TForm1.Image_settingClick(Sender: TObject);
begin
end;

procedure TForm1.Image_smileClick(Sender: TObject);
var
   MyTaskKillFileName:string;
   MyWideTaskKillFileName: array [0..256]of WChar;
begin
  //Application.Minimize;
  //Form1.WindowState:=wsMinimized;
  //ShowWindow(Self.Handle,SW_MINIMIZE);   //最小化窗口
  MyTaskKillFileName := ExtractFilePath(Paramstr(0)) +'bin\TASKMGR_ARM.exe';
  if FileExists(MyTaskKillFileName) then begin
     StringToWideChar(MyTaskKillFileName,MyWideTaskKillFileName,length(MyTaskKillFileName)+1);
     MyExecuteFile(MyWideTaskKillFileName);
  end;
end;


procedure TForm1.Image_systeminfoClick(Sender: TObject);
var
   MySystemInfoFileName:string;
   MyWideSystemInfoFileName: array [0..256]of WChar;
begin
   MySystemInfoFileName := ExtractFilePath(Paramstr(0)) +'bin\Resinfo.exe';
   if FileExists(MySystemInfoFileName) then begin
      StringToWideChar(MySystemInfoFileName,MyWideSystemInfoFileName,length(MySystemInfoFileName)+1);
      MyExecuteFile(MyWideSystemInfoFileName);
   end;
end;


procedure TForm1.Button1Click(Sender: TObject);
begin
  close;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
   MyIniFile:TIniFile;
   MyIniFileName:string;
   {MyBackgrndPicName,}MyKailidePicName,MyGaodePicName:string;
   MyDeskTopPicName,MyTaskMgrPicName:string;
   MyWelcomePicName:string;
   MyIsLoadCustomeSetting:string;
begin
  Form1.Caption := 'MULITYGPSSTARTER';
  Form1.DoubleBuffered:= True; //fix image painting
  //Application.ShowMainForm:=False;
  //set current time on label
  Label_DateTime.Caption:=DateToStr(now)  +' '+ formatdatetime('hh:mm',time);
  //load custom pictures from ini file
  MyIniFileName := ExtractFilePath(Paramstr(0)) +'Config.ini';
  //showmessage(MyIniFileName);
  if (FileExists(MyIniFileName)) then begin
    MyIniFile := TIniFIle.Create(MyIniFileName);
    MyIsLoadCustomeSetting := MyIniFile.ReadString('Global','IsLoadCustomeSetting','NO');
    if (MyIsLoadCustomeSetting ='YES') then begin
      // showmessage(MyIniFileName);
      //MyBackgrndPicName := MyIniFile.ReadString('Setting','BackgrndPicName','');
      MyKailidePicName  := MyIniFile.ReadString('Setting','KailidePicName','');
      MyGaodePicName    := MyIniFile.ReadString('Setting','GaodePicName','');

      MyDeskTopPicName  := MyIniFile.ReadString('Setting','DeskTopPicName','');
      MyTaskMgrPicName  := MyIniFile.ReadString('Setting','TaskMgrPicName','');
      MyWelcomePicName  := MyIniFile.ReadString('Setting','WelcomePicName','');

      //if (FileExists(MyBackgrndPicName)) then
         //Image_background.Picture.Bitmap.LoadFromFile(MyBackgrndPicName);
         //；
      if (FileExists(MyKailidePicName))then
         Image_kailide.Picture.Bitmap.LoadFromFile(MyKailidePicName);
      if (FileExists(MyGaodePicName)) then
         Image_gaode.Picture.Bitmap.LoadFromFile(MyGaodePicName);

      if (FileExists(MyDeskTopPicName)) then
         Image_desktop.Picture.Bitmap.LoadFromFile(MyDeskTopPicName);
      if (FileExists(MyTaskMgrPicName)) then
         Image_taskmgr.Picture.Bitmap.LoadFromFile(MyTaskMgrPicName);
      if (FileExists(MyWelcomePicName)) then
         Image_welcome.Picture.Bitmap.LoadFromFile(MyWelcomePicName);
    end;
  end;
  //hold := application.tittle;
  {if (ProcedureIsExists('MulityGPS.exe') )then begin
     showmessage('process is runnning!');
     application.terminate;
  end;}
end;

procedure TForm1.FormPaint(Sender: TObject);
begin
  //Application.ShowMainForm:=True;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  //Application.ShowMainForm:=True;
end;

end.

