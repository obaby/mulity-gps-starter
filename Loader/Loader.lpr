program Loader;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  {Interfaces, }// this includes the LCL widgetset
  {Forms, Unit1,}
  { you can add units after this }
  SysUtils,windows;

{$R *.res}

procedure MyExecuteFile(BinFileName:PWideChar);
var
   ShExecInfo: SHELLEXECUTEINFO;
begin
   ShExecInfo.cbSize:= sizeof(SHELLEXECUTEINFO);
   ShExecInfo.fMask:= SEE_MASK_NOCLOSEPROCESS;
   ShExecInfo.hwnd:= NULL;
   ShExecInfo.lpVerb:='';
   ShExecInfo.lpFile:=BinFileName;
   ShExecInfo.lpParameters:='';
   ShExecInfo.lpDirectory:='';
   ShExecInfo.nShow:= SW_SHOW;
   ShExecInfo.hInstApp := NULL;
   ShellExecuteEx(@ShExecInfo);
   //dwret:=WaitForSingleObject(ShExecInfo.hProcess,INFINITE);  //等待结束
   CloseHandle(ShExecInfo.hProcess);
end;

var
  PreviousHandle : THandle;
  KailideHandle : THandle;
  GaodeHandle : THandle;
  isNaviStarted : Boolean;
  MapbarHandle : THandle;
  MyKailideFileName:string;
  MyWideKailideFileName: array [0..256]of WChar;
  MyGaodeFileName:string;
  MyWideGaodeFileName: array [0..256]of WChar;
  MyMapBarFileName:string;
  MyWideMapBarFileName: array [0..256]of WChar;

begin
    //Form1.Caption := 'MULITYGPSSTARTER';   CARELAND_NAVIGATION
  isNaviStarted := False;
  GaodeHandle := FindWindow('Rousen',nil);   //窗口类名Rousen 窗口名ROUSEN
  if GaodeHandle <> 0 then begin
     MyGaodeFileName := ExtractFilePath(Paramstr(0)) +'Autonavi\RouCE6.exe';
     StringToWideChar(MyGaodeFileName,MyWideGaodeFileName,length(MyGaodeFileName)+1);
     MyExecuteFile(MyWideGaodeFileName);
     isNaviStarted := True;
  end;
  KailideHandle := FindWindow('CARELAND_NAVIGATION',nil);   //窗口类名 CARELAND_NAVIGATION 窗口名  NaviOne
  if KailideHandle <> 0 then begin
     isNaviStarted := True;
     MyKailideFileName := ExtractFilePath(Paramstr(0)) +'NaviOne\NaviOne.exe';
     StringToWideChar(MyKailideFileName,MyWideKailideFileName,length(MyKailideFileName)+1);
     MyExecuteFile(MyWideKailideFileName);
  end;
  MapbarHandle := FindWindow('Navi AppClass',nil); //  窗口类名 Navi AppClass 窗口名  NaviApp
  if MapbarHandle <> 0 then begin
     MyMapBarFileName := ExtractFilePath(Paramstr(0)) +'mapbar\MapbarNavi.exe';
     StringToWideChar(MyMapBarFileName,MyWideMapBarFileName,length(MyMapBarFileName)+1);
     MyExecuteFile(MyWideMapBarFileName);
     //SysUtils.ExecuteProcess('\SDMMC\mapbar\MapbarNavi.exe',[]);
     isNaviStarted := True;
  end;
  PreviousHandle := FindWindow(nil,'MULITYGPSSTARTER');
  if ((PreviousHandle = 0) and (isNaviStarted =False ))then begin //如果没有找到启动器窗口 并且没有启动任何的导航窗口
     SysUtils.ExecuteProcess('\StorageCard\MulityGps_Nodetect.exe',[]);
  end else begin
      if (isNaviStarted = False) then begin         //如果导航没有启动则显示选择窗口
         SetForegroundWindow(PreviousHandle);
      end;
  end;
end.

