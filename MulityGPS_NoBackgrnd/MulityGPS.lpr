program MulityGPS;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, Unit1, runtimetypeinfocontrols ,
  Windows //add by obaby
  //CheckPrevious in 'CheckPrevious.pas'
  { you can add units after this };

{$R *.res}

var
  PreviousHandle : THandle;
  KailideHandle : THandle;
  GaodeHandle : THandle;
  isNaviStarted : Boolean;
  MapbarHandle : THandle;

begin
  //Form1.Caption := 'MULITYGPSSTARTER';   CARELAND_NAVIGATION
  isNaviStarted := False;
  GaodeHandle := FindWindow('Rousen',nil);   //窗口类名Rousen 窗口名ROUSEN
  if GaodeHandle <> 0 then begin
     SetForegroundWindow(GaodeHandle);
     //Application.terminate;
     isNaviStarted := True;
  end;
  KailideHandle := FindWindow('CARELAND_NAVIGATION',nil);   //窗口类名 CARELAND_NAVIGATION 窗口名  NaviOne
  if KailideHandle <> 0 then begin
     SetForegroundWindow(KailideHandle);
     //Application.terminate;
     isNaviStarted := True;
  end;
  MapbarHandle := FindWindow('Navi AppClass',nil); //  窗口类名 Navi AppClass 窗口名  NaviApp
  if MapbarHandle <> 0 then begin
     SetForegroundWindow(MapbarHandle);
     isNaviStarted := True;
  end;
  PreviousHandle := FindWindow(nil,'MULITYGPSSTARTER');
  if ((PreviousHandle = 0) and (isNaviStarted =False ))then begin //如果没有找到启动器窗口 并且没有启动任何的导航窗口
     RequireDerivedFormResource := True;
     Application.Initialize;
     Application.CreateForm(TForm1, Form1);
     Application.Run;
  end else begin
      if (isNaviStarted = False) then begin         //如果导航没有启动则显示选择窗口
         SetForegroundWindow(PreviousHandle);
      end;
  end;

end.

